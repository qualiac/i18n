## Twine and Your Build Process

### Xcode

It is easy to incorporate Twine right into your iOS and OS X app build processes.

1. In your project folder, create all of the `.lproj` directories that you need. It does not really matter where they are. We tend to put them in `Resources/Locales/`.
2. Run the [`generate-all-localization-files`](#generate-all-localization-files) command to create all of the `.strings` files you need in these directories. For example,

```
if which twine >/dev/null; then
    curl -o twine.txt https://gitlab.com/qualiac/i18n/raw/master/twine.txt
    twine generate-all-localization-files twine.txt Inventories/Resources/ --tags qam-inventories --tags ios
    twine generate-localization-file twine.txt QKRString+extension.swift --format qkr_string_swift --tags qam-inventories --tags ios
    rm twine.txt
else
    echo "warning: Twine not installed"
fi
```

	Make sure you point Twine at your data file, the directory that contains all of your `.lproj` directories, and the tags that describe the definitions you want to use for this project.
3. Drag the `Resources/Locales/` directory to the Xcode project navigator so that Xcode knows to include all of these `.strings` files in your build.
4. In Xcode, navigate to the "Build Phases" tab of your target.
5. Click on the "Add Build Phase" button and select "Add Run Script".
6. Drag the new "Run Script" build phase up so that it runs earlier in the build process. It doesn't really matter where, as long as it happens before the resources are copied to your bundle.
7. Edit your script to run the exact same command you ran in step (2) above.

Now, whenever you build your application, Xcode will automatically invoke Twine to make sure that your `.strings` files are up-to-date.

### Android Studio/Gradle

#### Standard

Add the following code to `app/build.gradle`:

```
task generateLocalizations {
	String script = 'if hash twine 2>/dev/null; then twine generate-all-localization-files ../twine.txt ./src/main/res/ --format android; fi'
	exec {
		executable "sh"
		args '-c', script
	}
}

preBuild {
	dependsOn generateLocalizations
}
```